<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();
Route::resource('/', App\Http\Controllers\HomeController::class);
Route::group(['middleware' => ['auth']], function () {
  Route::resource('/listbarang', App\Http\Controllers\ListbarangController::class);
  Route::resource('/listtransaksi', App\Http\Controllers\ListtransaksiController::class);
  Route::resource('/inputtransaksi', App\Http\Controllers\InputtransaksiController::class);
});
